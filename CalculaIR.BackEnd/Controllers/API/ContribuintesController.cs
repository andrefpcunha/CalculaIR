﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CalculaIR.BackEnd.DataContext;
using CalculaIR.BackEnd.Models;

namespace CalculaIR.BackEnd.Controllers.API
{
    [RoutePrefix("api/contribuinte")]
    public class ContribuintesController : ApiController
    {
        private APIContext db = new APIContext();

        [Route()]
        // GET: api/contribuinte
        public IQueryable<Contribuinte> GetContribuintes()
        {
            return db.Contribuintes.OrderBy(Contribuinte => Contribuinte.VlImposto).ThenBy(Contribuinte => Contribuinte.Nome);
        }

        [Route()]
        // GET: api/contribuinte?id=1
        [ResponseType(typeof(Contribuinte))]
        public async Task<IHttpActionResult> GetContribuinte(int id)
        {
            Contribuinte contribuinte = await db.Contribuintes.FindAsync(id);
            if (contribuinte == null)
            {
                return NotFound();
            }

            return Ok(contribuinte);
        }

        [Route()]
        // POST: api/contribuinte
        [ResponseType(typeof(Contribuinte))]
        public async Task<IHttpActionResult> PostContribuinte(ContribuinteViewModel VM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Contribuintes.Add(VM.Contribuinte);
            await db.SaveChangesAsync();

            return Json("");// CreatedAtRoute("DefaultApi", new { id = VM.Contribuinte.ID }, VM);
        }

        [Route()]
        // PUT: api/contribuinte
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutContribuinte(ContribuinteViewModel VM)
        {

            //decimal ValorIR = Convert.ToDecimal(CalculaAliquota(VM.SalarioMinimo, 1000, 1));

            try
            {
                var imposto = db.Contribuintes;

                foreach (var item in imposto)
                {
                    var cont = db.Contribuintes.Find(item.ID);

                    cont.VlImposto = Convert.ToDecimal(CalculaAliquota(VM.SalarioMinimo, item.RendaBruta.Value, item.NrDependentes));
                    //await db.SaveChanges();
                }
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private double? CalculaAliquota(decimal SalarioMinimo, decimal Renda, int dependentes)
        {
            double? Aliquota = 0;
            double? Imposto = 0;

            if (SalarioMinimo > 0)
            {
                decimal? RendaLiquida = Renda - (((SalarioMinimo * 5) / 100) * dependentes);

                if (RendaLiquida >= 0 && RendaLiquida <= (SalarioMinimo * 2))
                {
                    Aliquota = 0;
                    Imposto = 0;
                }
                else if (RendaLiquida > (SalarioMinimo * 2) && RendaLiquida <= (SalarioMinimo * 4))
                {
                    Aliquota = 7.5;
                    Imposto = (Convert.ToDouble(RendaLiquida) * Aliquota)/100;
                }
                else if (RendaLiquida > (SalarioMinimo * 4) && RendaLiquida <= (SalarioMinimo * 5))
                {
                    Aliquota = 15;
                    Imposto = (Convert.ToDouble(RendaLiquida) * Aliquota) / 100;
                }
                else if (RendaLiquida > (SalarioMinimo * 5) && RendaLiquida <= (SalarioMinimo * 7))
                {
                    Aliquota = 22.5;
                    Imposto = (Convert.ToDouble(RendaLiquida) * Aliquota) / 100;
                }
                else if (RendaLiquida > (SalarioMinimo * 7))
                {
                    Aliquota = 27.5;
                    Imposto = (Convert.ToDouble(RendaLiquida) * Aliquota) / 100;
                }
                else
                {
                    Aliquota = 0;
                    Imposto = (Convert.ToDouble(RendaLiquida) * Aliquota);
                }
            }

            return Imposto;
        }
    }
}