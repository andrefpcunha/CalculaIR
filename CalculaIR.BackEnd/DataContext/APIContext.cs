﻿using CalculaIR.BackEnd.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CalculaIR.BackEnd.DataContext
{
    sealed class APIContext : DbContext
    {
    
        public APIContext() : base("name=CalcularIRContext")
        {
        }

        public DbSet<Contribuinte> Contribuintes { get; set; }
    }
}
