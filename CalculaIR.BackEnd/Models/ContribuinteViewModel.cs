﻿using System;

namespace CalculaIR.BackEnd.Models
{
    public class ContribuinteViewModel
    {
        public Contribuinte Contribuinte { get; set; }

        public decimal SalarioMinimo { get; set; }

        public ContribuinteViewModel()
        {
            Contribuinte = new Contribuinte();
        }
    }
}