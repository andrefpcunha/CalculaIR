namespace CalculaIR.BackEnd.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Contribuinte")]
    public partial class Contribuinte
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Nome { get; set; }

        [Required]
        [StringLength(11)]
        public string CPF { get; set; }

        public int NrDependentes { get; set; }

        public decimal? RendaBruta { get; set; }

        public decimal? VlImposto { get; set; }
    }
}
