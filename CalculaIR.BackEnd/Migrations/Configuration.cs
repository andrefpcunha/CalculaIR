namespace CalculaIR.BackEnd.Migrations
{
    using CalculaIR.BackEnd.DataContext;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<APIContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(APIContext context)
        {
            
        }
    }
}
