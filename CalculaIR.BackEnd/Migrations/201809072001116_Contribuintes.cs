namespace CalculaIR.BackEnd.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Contribuintes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contribuinte",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 50),
                        CPF = c.String(nullable: false, maxLength: 11),
                        NrDependentes = c.Int(nullable: false),
                        RendaBruta = c.Decimal(precision: 18, scale: 2),
                        VlImposto = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Contribuinte");
        }
    }
}
