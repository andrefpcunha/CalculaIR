﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using CalculaIR.BackEnd.Controllers.API;
using CalculaIR.BackEnd.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CalculaIR.BackEnd.Tests.Controllers
{
    [TestClass]
    public class ContribuintesControllerTest
    {
        [TestMethod]
        public void Get()
        {
            ContribuintesController controller = new ContribuintesController();

            IQueryable<Contribuinte> result = controller.GetContribuintes();
        }

        [TestMethod]
        public void GetById()
        {
            ContribuintesController controller = new ContribuintesController();
            Task<IHttpActionResult> result = controller.GetContribuinte(1);
        }

        [TestMethod]
        public async void Post()
        {
            ContribuintesController controller = new ContribuintesController();
            ContribuinteViewModel contribuinte = new ContribuinteViewModel();

            await controller.PostContribuinte(contribuinte);

        }

        [TestMethod]
        public async void Put()
        {
            ContribuintesController controller = new ContribuintesController();
            ContribuinteViewModel contribuinte = new ContribuinteViewModel();

            await controller.PutContribuinte(contribuinte);
        }
        
    }
}
