################################
Programa para Calculo do IR
################################


# Resumo de uso do sistema:
	- O usuário insere as informações do contribuinte; 
		em seguida, o sistema habilita o campo para informar o valor do Salário mínimo para que seja listado os contribuintes cadastrados;
	- A lista retorna os valores em Ordem crescente de valor do Imposto e ordem crescente de nome;
	* O sistema não faz nenhum tipo de validação de dados;
	

# COMO EXECUTAR O PROJETO?
	-Será necessário o uso das seguintes ferramentas:
		* VS Studio 2017 
		* Microsoft SQL Server 2012
	
	-Abrir o projeto "CalculaIR.sln" no VS Studio 2017
	-Executar o script "Gerar_BaseDeDados_calculair"


# Frameworks utilizados:
	- MVC .NET 4.6.2
	- Entity V. 6.0
	- Razor  V. 3.0
	- DataTables Plug-in
	- Bootstrap  V. 3.3.7
	
	
# Arquitetura:
	- Backend: REST
	- FrontEnd: HTML + JavaScript + Bootstrap
	- Unit Test da classe ContribuinteController

